## C'est quoi le Design ?

Nous avons eu une i**ntroduction au Design** pour ce module qui nous a permis de comprendre les **enjeux et les objectif**s qui doivent etre atteints lors de la phase de **création d'un objet**. Chaque objet a une forme et une fonction particulière.

Plusieurs étapes clés du processus de création nous permettront d'y parvenir:

- DECONSTRUIRE L'OBJET, COMPRENDRE SA FONCTION : observer pour analyser, décrire et comprendre l'objet avec du recul.

- RESOUDRE LES PROBLEMES : mise en place des contraintes et limites qui composent l'objet.

- COMPRENDRE LES DIMENSIONS : ergonomie, poids, tailles, l'usage influe sur la forme.

- ESQUISSE, ESSAIS, ERREURS, AMELIORATION : prototype et maquette, expérimentations pour arriver à un objet fini (ou pas).

- ADAPTATION, DIFFUSION, EDITION : diffusion, production à la chaine.

- NOM, LOGO, PICTOGRAMME : représente l'objet implicitement pour le reconnaitre sans le voir. 





## Mon projet final:

Un **Capodastre** est un objet que tout les **musiciens** et spécialement les guitaristes connaissent, mais c'est quoi exactement ? pour faire simple en fait ca se place sur le manche d'une **guitare**, entre les cases/frettes (de 21 à 24 en générale, ca dépend du style de guitare) et ca permet de monter ou descendre de tonalité, plus on place le capodastre haut sur son manche plus on va jouer aigue. On s'en sert souvent pour adapter une musique à une voix par exemple ou bien juste éviter de trop faire la technique du 'barré' un peu fatiguante à la longue. Changer de tonalité peut aussi donner un style différent. Beaucoup donc l'utilisent, de nombreux types existent (selon l'épaisseur du manche de guitare par exemple). Mais ce n'est tout de meme pas un objet indispensable pour un guitariste d'un certain niveau, aussi question de style (celui qui va faire du Jazz n'en aura certainement pas besoin). 
Moi meme utilisant cet objet depuis de longue année, contre mon gré assez souvent, je peux dire qu'il pose beaucoup de questions. Ca ne fonctionne parfois pas très bien (certaines cordes ne sont pas assez 'pincés' (vibrage sur les frettes) et nécessite d’être constamment réajusté. 

Son design et son fonctionnement sont assez **fascinant** pour un si petit objet.
Il pourrait etre une bonne piste **d'exploration** pour une **évolution** dans le cadre du **Fablab**.


**UN CAPODASTRE PAR TYPE DE GUITARE ?**


Le capo ne sera pas le même pour une **guitare folk, acoustique, classique ou encore électrique**. En effet, cet accessoire qui se pose sur le manche doit être adapté à toutes les tailles.
Le manche de la guitare classique est le plus large et le plus épais, au contraire celui de la guitare électrique qui possède un manche très fin.
Attention également à ce qu’on appelle **le radius**. Ce terme est la mesure de l’angle du manche si celui-ci est bombé. Cette mesure est indispensable car la plupart des capodastres sont conçus pour des manches plats (flat).
Les séries de capo distinguent souvent ceux destinés aux guitares électrique et folk de ceux destinés aux guitares classiques. 

MON CAPO PERSONNEL:

![](images/capodastre.png) 

## Analyse de l'objet

Nous avons analysés de manière pragmatique notre objet personnel en petit groupe chacun à tour de rôle.

Qualités/Défauts/Analyses

- Matière rugueuse au niveau du maintient de la pince
- 3 matériaux différents: plastique, métal, caoutchou 
- Plastique, silicone  changeable mais pas assez stable
- Matière antidérapente au niveau des cordes
- Partie plus longue pour les cordes que pour le manche, économie de matière
- Les pinces sont courbes
- la courbe n'est pas efficace
- La structure de l'outil est faite d'un seul fil entier, en acier, et courbé
- Fonctionnalité 
- Les pinces se tordent à cause du ressort qui les soutient mal


## Composition de mon objet

Mon **Capodastre** est composé d'une **tige d'acier** souple d'un seul bloc, qui a été **déformée** selon la forme du manche d'une guitare électrique, et en ajoutant la forme d'un **ressort en torsio**n au centre de l'objet pour sa **souplesse**. Deux parties en **Caoutchou** viennent recouvrir les parties en contact direct avec le manche de la **guitare et les cordes**.

- fonction de mon objet:

maintenir les cordes du manche d'une guitare, exercer une pression constante sur celles ci.

- Problemes :

objet externe à la guitare, se perd facilement, difficilement adaptable à chaques type de guitare.

- Dimensions :

entre 10 et 14 cm, objet égal à la dimension du manche d'une guitare.

## ..

Rajout du caoutchouc en plus pour la surface de contact. Le caoutchouc est très solide et efficace malgré les utilisations répétés mais ne convient pas à l'armature en acier du Capo de base.
(le caoutchouc glisse et n'est pas stable) et fait un peu ''rajouté'' à la structure d'acier de base.

PK pas alors n'utiliser que du caoutchouc pour mon capo ?



## Premiers projets et prototypes: 

Pour mon projet j'ai choisi de rebondir sur ma question précédente: PK pas alors n'utiliser que du **caoutchouc** pour mon capo ?

Un manche de guitare est fait pour etre utilisé avec les mains, la peau est une surface douce qui n'abime pas le bois, ni les cordes. On retrouve sur tout les types de Capo du caoutchouc comme surface de contact direct avec les cordes. Le Caoutchouc est un matériau souple, robuste et doux.

- Je suis allé voir un **professionnel de la fabrication** est transformation de ce matériaux afin qu'il m'explique ses nuances, atouts et faiblesses et surtout qu'il me donne quelques échantillons.

![](images/promaga.png)


## PREMIER PROTOTYPE 

- J'ai testé la matière **caoutchouc** en la découpant, en effectuant des noeuds, des trous, pour voir son **comportement**. 


- J'ai tenté d'effectuer un **serrage** autour du manche de ma guitare pour **imiter le fonctionnement d'un vrai capo**. 

CONCLUSION:

- Au début le résultat est mitigé, la **pression d'appuis** sur les cordes du caoutchouc n'est pas suffisante pour que celles-ci sonnent bien. 

![](images/cp00.png)

- J'ai décidé d'imprimer à l'aide de l'imprimante 3D une **pièce longitudinale** qui permettrait de **solidifier et de maintenir** la partie supérieur de mon lien en caoutchouc. 

![](images/cp0.png)

## DEUXIEME PROTOTYPE

- Je cale cette pièce en **sandwitch entre deux languettes de Caoutchouc**. Voici un nouveau **proto**.

![](images/cp1.png)

![](images/cp2.png)

![](images/cp5.png)

- Le résultat est beaucoup mieux, il y a une **pression uniforme** sur le manche et les cordes. Il faut cependant **appliquer une force importante** sur le **noeud** lors de l'attache. Pas évident. 

![](images/cp4.png)

![](images/cp3.png)

![](images/cp6.png)

- Je décide de **jouer** un ou deux morceaux, je réalise une video qui permet de voir **le capo en situation**, je change de case en cours de video.

![](images/First_playing_with_my_caoutchouc_Capo.mp4)

CONCLUSION:

- Mon analyse post essai et que **le son sort bien**, le **maintient est efficace** mais il est assez difficile de bouger rapidement le Capo, de le détacher et réatacher avec une **pression suffisante**. J'ai un **surplus de matière** qui ne sert a rien.

![](images/cp3.png)

## TROISIEME PROTOTYPE

- Je dessine un schema qui pourrait correspondre à un **assemblage** de matière fonctionnel. Je **dimentionne** ma création.

![](images/schemaproto1.png)

- Je fonce au Fablab faire imprimer et découper pour pouvour effectuer un **nouveau proto**.

J'ai pu discuter avec Axel qui m'a confirmé l'utilisation de la **découpeuse laser Lasersaure pour le Caoutchouc**. Les bonnes vitesses et puissances nécessaire, le nombre de passes etc

- Je fais des tests de découpe de bandelettes de Caoutchouc avec la **Lasersaure**, je trouve les bonnes vitesses et puissance par rapport à l'épaisseur des 4mm de mon matériaux. La découpe laser se déroule bien, je dois programmer deux passes pour avoir une découpe optimale.

- J'imprime avec l'imprimante 3D deux pièces de **PLA** que je vais ensuite rassemblés lors de la jonction avec la languette de Caoutchouc.


![](images/bandelettelase.png)

Je manques de temps, j'ai pu tester seulement une poignée de seconde le résultat de cet essai. La pièce 3D que j'ai imprimé n'est pas assez solide est n'a pas **résistée à la tension du caoutchouc**. Le fait d'avoir deux pieces de **PLA** séparés puis recolées par la suite a crée une fragilité. Je suis cependant satisfait de **l'assemblage** de l'ensemble **(pièce 3D et caoutchouc)** qui permet de **rigidifier la partie en contact avec les cordes**. Pour ce prototype je vais maintenant réimprimer en 3D  à la bonne echelle puis découper des **languettes de caoutchouc** avec la **laser** pour voir le temps de découpe et leurs **épaisseurs équilibrée**.


![](images/bullecercle.png)


CONCLUSION:

Le système d'attache est trop complexe et trop difficile à **serer et à déplacer rapidement**. Il n'y a pas la possibilité **d'ajuster correctement la tension** du caoutchouc de serrage. Le fait d'avoir deux pieces de PLA séparés puis recolées par la suite a crée une fragilité. Une partie de bandelette de mon lien en Caoutchouc ne sert à rien.
Il me faut imaginer une sorte de **système cranté** pour l'attache.
Celui-ci doit pouvoir se **placer et se déplacer** plus aisément.

## QUATRIEME PROTOTYPE °Préjury

![](images/schemaproto4.png)

- J'y suis, voici le résultat en vrai:

![](images/photoguitare1.png)

----

![](images/ccpp.png)

Résultat de l'apres midi !

## POST PREJURY

De nombreuses questions se sont alors posées à ce moment-là notamment sur la **solidité** du lien en élastique de mon capo. Le type de caoutchouc n'est pas assez **extensible et souple** donc cassé au moindre étirement un peu forcé. Le système de languette, son percement et sa forme ne sont pas adéquats. Je dois retravailler le fond de mon projet pour qu'il soit plus lisible et compréhensible.

## Phase 1

Je reprécise mes objectifs et mon propos:

Je veux réaliser un Capodastre **reproduisible** grâce à un fablab, il ne s'adresse pas forcément à une cible professionnelle de la guitare, mais plus à un grand public voulant une sorte de **couteau suisse** du Capodastre, _tu l'as toujours avec toi et il fonctionne avec toutes tes guitares_ , oui il peut **s'adapter** sur tous les types de guitares, il a une matière **recyclable** et de **récup** comme le **caoutchouc**, qui Pourrat être **interchangeable** pour une **durée de vie supérieure**, il devra aussi être **réglable parfaitement** pour que la **pression sur les cordes** puisse être adaptée comme on le souhaite. Le capo doit être **petit et pas encombrant**, il peut être **placé dans sa poche** et maintenir au moins **deux ou trois médiators** avec. Et surtout, avec une attache parfaitement solide et durable, pour que l'ensemble soit cohérent.


## Et donc...

-Je change donc de caoutchouc vers du quatre mm plus naturel, plus souple et extensible.

-Je redessine de nouvelles languettes puis j'effectue de nombreux tests de découpes.

-Je revois ma pièce de PLA en profondeur, ainsi que sa courbure.

## Phase 2 

J'utilise la lasersaure, j'effectue des découpes et je fais des tests de résistance de la matière et des striures de ma languette de Caoutchouc.
Mon but: avoir la matière la plus extensible tout en gardant de la solidité et une simplicité de mise en place pour le guitariste.

J'étudie ce qu'est le **Radius** d'une guitare: Le radius, c’est la courbure que présente la touche d’une guitare. Plus le cercle est grand, plus la touche tend vers le plat. Ainsi, un cercle ayant un rayon (radius) de 7,25 pouces est plus petit et donc plus courbé qu’un cercle d’un rayon de 9,5 pouces. Autrement dit, plus le radius est petit, plus la touche est courbée. Par contre, plus il est élevé, plus la touche est plate.
J'ai demandé à un professionnel des instruments bois et il m'a dit que le radius d'une guitare variait entre 7.25 et 20



QUELQUES PHOTOS DE MON AVANCEMENT :


![](images/n.png)

![](images/b.png)

## Liens

- [CAPO N1.stl](images/capoenstl.stl)

- [CAPO N4.stl](images/capov.stl)

- [Propriétés Caoutchouc](https://www.caoutchouc.qc.ca/categories_caoutchouc.php)

FINAL 

- [CAPO PLA FINAL](images/CapoPLA.stl)

- [CAPO CAOUTCHOUC DWG](images/CapoCAOUTCHOUC.dwg)

- [CAPO CAOUTCHOUC SVG](images/CapoCAOUTCHOUC.svg)


