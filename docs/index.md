## Qui suis-je ?

![](images/img_4175.jpg) 



Salut, moi c’est **Valère Santarelli**, j’ai 25 ans et je suis étudiant en Master à la **faculté d’architecture de la Cambre Horta à Bruxelles**.

## Mon background !!

Je suis Français, née à **Ajaccio en Corse** où j’y ai vécu jusqu’à mes 18 ans, âge auquel j’ai obtenu mon diplôme du Bacalauréat. J’ai a ce moment la décidé de partir vers de nouveaux horizon, j’ai voyagé entre l**’Angleterre et l’Italie** pendant plusieurs mois, des expériences enrichissantes qui m’ont permis de découvrir de nouvelles cultures et langues. Voulant éveiller mon coté **créatif** je me suis entre temps tourné vers des **études d’architecture** en intégrant La Cambre Horta à Bruxelles, ces études qui m’on menés aujourd’hui en **Master**. 

Pour compléter ma personnalité, je suis assez **manuel** et j’aime **créer**, **construire** et **réparer** tout ce qui m’entoure. J’aime tout ce qui touche à la mer, je voulais devenir **architecte naval** étant petit. 
Mes hobbies sont la musique, les randonnées sportives et le foot. J’aime aussi la déco intérieur, l’aménagement des espaces et le mobilier contemporain.

J’ai pour cette année 2021-2022 choisi l’atelier ‘’**TERRAINS**’’.

## Mes projets

Mon objet...
Le processus de création de mon objet en bois ci dessous, je voudrais utiliser ce projet comme référence a mon futur objet.
Le principe ici était de partir d'une matière de base étant des chutes de toutes sorte de bois et de les retravailler pour creer des patchworks en y incrustant des motifs, calpinages et percements a l'aide des machines numérique.





![This is another caption](images/Objet-bois.jpg)

## Mon nouveau projet:

Un **Capodastre** est un objet que tout les **musiciens** et spécialement les guitaristes connaissent, mais c'est quoi exactement ? pour faire simple en fait ca se place sur le manche d'une **guitare**, entre les cases/frettes (de 21 à 24 en générale, ca dépend du style de guitare) et ca permet de monter ou descendre de tonalité, plus on place le capodastre haut sur son manche plus on va jouer aigue. On s'en sert souvent pour adapter une musique à une voix par exemple ou bien juste éviter de trop faire la technique du 'baré' un peu fatiguante à la longue. Changer de tonalité peut aussi donner un style différent. Beaucoup donc l'utilisent, de nombreux types existent (selon l'épaisseur du manche de guitare par exemple). Mais ce n'est tout de meme pas un objet indispensable pour un guitariste d'un certain niveau, aussi question de style (celui qui va faire du Jazz n'en aura certainement pas besoin). 
Moi meme utilisant cet objet depuis de longue année, contre mon gré assez souvent, je peux dire qu'il pose beaucoup de questions. Ca ne fonctionne parfois pas très bien (certaines cordes ne sont pas assez 'pincés' (vibrage sur les frettes) et nécessite d’être constamment réajusté. 

Son design et son fonctionnement sont assez **fascinant** pour un si petit objet.
Il pourrait etre une bonne piste **d'exploration** pour une **évolution** dans le cadre du **Fablab**.


![](images/capodastre.png) 







