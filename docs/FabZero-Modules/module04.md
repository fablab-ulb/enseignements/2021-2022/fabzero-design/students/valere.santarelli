# Module 4. Découpe assistée par ordinateur


Pour ce module nous avons eu une formation pratique et théorique sur l'utilisation des découpeuses laser et de la découpeuse vinile, puis un travail de création. 

Dans un premier temps je vais vous exposer **l'utilisation de la découpeuse laser et ses aspects technique**, avec la création d'une bandelette en polypropylène pour tester les différentes puissances de découpe et de gravure avec la **Lasersaur**.
Puis dans un second temps, la présentation de mon travail de **création d'une lampe** avec toutes mes expérimentations. 

_A retenir surtout, une liste de matériaux a utiliser ou pas:_

**UTILISABLES:**

-bois contreplaqué de 3 ou 4 mm idéalement carton

-papier

-acrylique

-certains textiles

**DECONSEILLES:**

-MDF

-ABS, PS

-les métaux

-polyétylène épais PE, PET, PP

**PROSCRITS:**

-Téflon

-le PVC

-le cuivre

-le vinyle, cuir

## MACHINES

![](../images/sample-hoto.jpg)
**_LASER CUTTER - LASERSAUR_**

Machine de grande taille:

Surface de découpe : 122 x 61 cm

Hauteur maximum : 12 cm

Vitesse maximum : 6000 mm/min

Puissance du LASER : 100 W

Type de LASER : Tube CO2 infrarouge

- Issue d'une fabrication artisanale made in fablab ULB, elle fonctionne avec le programme **INKSCAPE** et avec des fichiers sous format **SVG** 


**_LASER CUTTER - EPILOG_**

Machine avec un format plus compacte:

Surface de découpe : 81 x 50 cm

Hauteur maximum : 31 cm

Puissance du LASER : 60 W

Type de LASER : Tube CO2 infrarouge


- De conception plus moderne, elle est dotées dʼun panneau dʼaffichage à écran tactile, dʼun positionnement de lʼillustration à lʼécran.

**_Nota bene_**

Il faut au préalable s'inscrire et réserver un crénaux horaire sur le site dédié _FABMAN_ pour l'utilisation des machines. Un QR code permettra de déverrouiller les machines à l'heure de le réservation.

_Avant de commencer !_

[v] Connaître avec certitude quel matériau est découpé

[v] Toujours activer l’air comprimé

[v] Toujours allumer l’extracteur de fumée

[v] Savoir où est le bouton d’arrêt d’urgence

[v] Savoir où trouver un extincteur au CO2

[v] Rester à proximité de la machine jusqu'à la fin de la découpe

[v] Ne pas regarder fixement l'impact du faisceau LASER

[v] Après une découpe, ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur



## EXERCICE 1: Réalisation d'une bandellette de références

Nous avons utilisé le logiciel **Illustrator** pour tracer notre patron de **bandelette** divisée en 7 partie équivalentes à différente vitesse de laser **(entre 100 et 2300)**, on garde cependant la meme puissance pour l'ensemble **(10 Watt)**. Cela nous permettant par après de trouver le pliage adéquat pour notre projet en testant la résistance et la souplesse de chacune des striures.
Nous avons eu quelques soucis lors de l'export via illustrator vers le logiciel laser, il faut faire attention au bon format et aux couleurs de chaque trait de notre dessins, écritures, tailles, avant de l'exporter. 

On passe alors à la découpe. On importe notre fichier en svg, on règle la puissance, la vitesse, pour chaque partie de notre bandelette. On place bien notre matériaux a découper dans la machine, on l'attache, on vérifie que la laser est bien en place, et on lance la découpe.
![](../images/bandelette1.png)
-----![](../images/band1.png)-----

- On encode chaque paramètres de **vitesse et de puissance** pour chaques couleurs avec le programme.

![](../images/tabl.png)


- Puis on place notre **feuille de polypropylène** correctement placé et maintenue dans la machine.

- Tout le processus d'allumage est de **configuration** de la Lazersaure commence alors :

![](../images/gd1.png)
![](../images/gd2.png)


## Résultat 

- Cinq petite minutes de découpe plus tard...

**ET VOILA LE RESULTAT DE CE PREMIER TRAVAIL !!**





![](../images/bandelette2.png)





## EXERCICE 2: Création d'une lampe avec du Polypropylène

Pour la réalisation de **ma lampe**, je me suis inspiré de l'art du **Kirigami** qui est, dans la culture japonaise, l'art du découpage du papier, il se différencie de l'origami par l'absence de pliage. Mon but est alors de creer une sorte de **matière auxétique** en dessinant un **Pattern** qui va permettre l'étirement de celle-ci.
Grace a cela, mon abat jour aura des ouvertures plus ou moins grande et une **forme évolutive** selon la **force d'étirement** appliquée.
J'ai fait plusieurs expérimentations de Patterns pour trouver le plus adéquat.

**Mon cahier des charges:**


-Creer une applique murale ou applique de sol, en longueur, avec une lumière diffuse

-Je dispose d'un socket longitudinal, style a l'ancienne

**Problématiques rencontré:**

- Trouver le motif le plus adapté à la solidité de mon abat jour
- Avoir un sens des jonctions cohérent avec le pliage
- Trouver le motif le plus adapté au bon sens d'étirement de mon abat jour
- Trouver un motif visuelement graphique
- Avoir une belle diffusion de lumière ciblé


**Etude**

Je me suis informé sur les Kirigami, les materiaux Auxetic, les métamateriaux mecaniques.
Mon but était de trouver la forme de Pattern la plus adéquat à l'étirement, sans cassures et solide.

_AUTOCAD/ILLUSTRATOR_

Une convertion DWG to SVG sera obligatoire pour la Lasercut.

-trait rouge = gravure

-trait jaune = découpe intérieur 

-trait vert = découpe 


![](../images/tota.png)

**Problèmes au début:**

Je n'avais pas encore de cahier des charge, je voulais juste faire des motifs, j'ai pris le probleme a l'envers, j'ai trouvé un socket après avoir commencé mes expérimentations, je me suis retrouvé avec ce socket sans savoir comment l'adapter à mon abat jour, je n'ai pas regarder le projet dans sa globalité

_EXPERIMENTATIONS_

![](../images/tot.png)

_FINAL_

![](../images/finallampe.png)

**Problèmes finaux et conclusion**

Surement, pas la bonne matière.
Je n'ai pas utilisé le bon Pattern selon mon sens de traction, il y a des irrégularités dans ma forme, je n'ai pas fait le bon choix de source lumineuse par rapport à mon abat jour (forme longitudinale aurait été plus adaptée).
L'effet de percements n'est pas pur.

Je suis cependant satisfait de mes expérimentations, j'ai appris beaucoup sur la matière et sur les formes **auxetic**
De nombreux échanges et partages constructifs avec d'autres étudiants.

**EXPO**

![](../images/videoexpo.mp4)





## FICHIERS ET LIENS UTILES

[INKSCAPE](https://inkscape.org/release/inkscape-1.1.1/)
- 
[FABMAN](https://fabman.io)
- 
[CONVERTER FILES](https://cloudconvert.com/dwg-converter)
- 
[EPILOGLASER](https://www.epiloglaser.fr/fonctionnement/applications-laser.htm)
- 
[fichier.svg](../images/derv.svg)
- 
[fichier.dwg](../images/derv2.dwg)
