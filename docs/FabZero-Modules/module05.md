# Module 5. SHAPER

Cette semaine nous avons eu une formation théorique et pratique concernant l'utilisation de la **SHAPER** qui est une machine de fraisage CNC guidé à la main.

## Utilisation 

Axel nous a expliqué comment fonctionnait la Shaper et quels étaient les types d'utilisations les plus adéquats selon nos travaux à effectuer.
Nous avons expérimenté manuelement la machine

La machine est dotée de deux poignées directionnels, dʼun écran tactile et dʼune caméra en dessous. Elle mesure 349.3 x 196.9 mm avec une hauteur de 298.5 mm pour un poids total de 6,6 kg.

## Réalisation de la découpe

1. Fixer sur mon plan de travail la planche que je découpe avec du Tesa double face. Le tout doit etre totalement stable.

2. Placer le Shaper Tape sur ma surface de découpe intelligeament.

3. Brancher l'aspirateur

4. Scanner la zone de travail

5. Importer mon fichier SVG sur USB et brancher USB sur la machine










## Deuxième partie

![](../images/planshaper.png)

SOURCE:www.shapertools.com

![](../images/shaper2.png)

## Liens utiles


- [SITE SHAPER](https://www.shapertools.com/fr-fr/origin/spec)




