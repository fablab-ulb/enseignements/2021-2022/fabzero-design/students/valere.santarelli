# Module 3. Impression 3D
Cette semaine nous avons suivi une formation sur la **réalisation d'objet 3D** et **l'utilisation des imprimantes 3D** de la marque **PRUSA**.
Nous devions **réaliser notre objet personnel** ou une partie de celui ci grace aux imprimantes 3D.
J'ai choisi de faire mon **vide poche en bois**, en réduisant son echelle et en ré-adaptant les bordures.


## PRUSA
J'ai téléchargé le programme PRUSA, https://www.prusa3d.com/drivers/
Puis j'ai séléctionné les machines que nous utilisons au FabLab: ORIGINAL PRUSA I3 MK3S & MK3S+ ORIGINAL PRUSA I3 MK3

## Importer/Exporter

A partir du programme FUSION 360 j'ai exporter mon objet directement en format STL.
J'ai ensuite ouvert ce fichier STL dans le logiciel PRUSA.

## Configuration pré-print

J'ai ici configuré la taille et les épaisseurs de mon objet directement dans Prusa Slicer selon les bonnes configurations et pour optimiser au maximum le temps d'impression. Voici les étapes:

- Importer le modèle en _.stl_
- Adapter l'echelle du modèle avec les poignées orange
- Régler les paramètres d'impression (supports, remplissages, bordures)
- Exporter le G-code

## Mon objet

Le **vide poche en bois** est trop grand pour etre imprimé sur le plateau de l'imprimante 3D, donc je vais réaliser une réplique en miniature pour comprendre comment fonctionne l'imprimante 3D ainsi que le logiciel Prusa.


## PRINT 3D

**VIDEO CONFIGURATION**

J'ai effectué une capture d'écran vidéo, vous pourrez donc suivre mon processus de programmation de mon objet directement sur l'interface.

![](../images/prusah1.mp4)


## Quelques photos

![](../images/1A_compressed.jpg)
![](../images/1B_compressed.jpg)
![](../images/1C_compressed.jpg)


**VIDEO IMPRESSION**


![](../images/videoprusa2.mov)

- Pour un total de 1h23 d'impression, la qualité de l'objet est à la hauteur de mes attentes.
- Je n'oublie pas de nettoyer la surface de la machine Prusa après mon utilisation avec le produit fourni et un essuis-tout.

****RESULTAT****

- Ce module m'a permis de comprendre la matière et son sens de fonctionnement, sa solidité.
- Quels types d'épaisseurs des bordures sont le plus adéquat.
- Les bon chiffres dans les grilles de configuration.
- Le type de remplissage adéquat. (nid d'abeille, grille, longitudinal)

![](../images/videpoche1.png)
![](../images/videpoche2.png)

## LIENS DIVERS

- [fichier.stl](../images/patswork v2.stl)
- 
- [fichier.gcode](../images/patsworkgcode.gcode)
- 
[PRUSA](https://www.prusa3d.com/drivers/)







