# Documentation module 1. GIT

Cette semaine nous avons commencé le **module 1**, dans un premier temps nous avons abordé des **notions de codage**, puis nous avons pris en mains l’interface de partage **Gitlab**, tout d’abord en créant un profil et un compte Gitlab puis en parcourant la plateforme. 

Dans un second temps nous avons commencés les premières **configurations du Git** dans le but de **documenter et partager** l’ensemble de nos travaux, expérimentations et connaissances tout au long du quadrimestre.
Le but de notre utilisation de Gitlab est de conserver une trace de notre apprentissage détaillé qui sera accessible à tous.


## 1.CONFIGURATION DE GITLAB

- [x] J’ai tout d’abord télécharger le logiciel **Gitlab** qui permet donc de travailler en groupe sur un même projet et de **documenter nos travaux**.



- [x] Pour cela j’ai ouvert le terminal de mon ordinateur, je travail sur Mac OS, j’ai donc appuyé sur **command + space** et j’ai écrit terminal qui est déjà installé sur mac sous le nom de **Bash**. 

- [x] J’ai en suite télécharger XCode en encodant  **xcode-select —install** dans le terminal bash.

 - [x] J’ai ensuite installé le logiciel **homebrew** en encodant **/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)** dans la ligne de commande, j’ai appuyé sur **enter**. 
 
 
- [x] J’ai encodé le **mot de passe de ma session utilisateur** de mon MacBook. Je le note dans un coin pour ne pas l’oublié. Ok, Homebrew a été installé.
Puis j’ai encodé dans le terminal **brew install git**


- [x] J’ai reçu un **numéro de id ssh: ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIS73/x/6II+oeWvXcqHU1IqncaGqUg42efgwzDE6x/f valere.santarelli@hotmail.fr**

Puis,

**Enter file in which to save the key (/Users/valeresantarelli/.ssh/id_ed25519):
/Users/valeresantarelli/.ssh/id_ed25519**


- [x] Cela étant fait, j’ai vérifié la version qui m’a été installé **git —version**

- [x] Par la suite j’ai encodé **mon adresse mail** (la même que sur mon compte gitlab) **git config —global user.name ‘’valere.santarelli**

- [x] Puis mon adresse mail **git config —global user.email valere.santarelli@hotmail.fr**, j’ai enfin vérifié ma configuration avec **git config —global —list**


Pour connecter mon ordinateur à Gitlab il m’a fallut rajouter des informations de sécurité et **configurer une clé SSH**.

 - [x] Je suis donc allé sur Gitlab, j’ai ouvert la clé crée précédemment et affichée sur mon terminal. 
 
 - [x] Je génère alors une **SSH key pair** en encodant **ssh-keygen -t ed25519 -c valere.santarelli@hotmail.fr**
 
- [x] Puis j’ai sauvé la clé **(/home/user/.ssh/id_ed25519:**

- [x] J'ai enfin **copié/collé**  **ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIS73/x/6II+oeWvXcqHU1IqncaGqUg42efgwzDE6x/f valere.santarelli@hotmail.fr** dans la **case SSH** qui se trouve dans les paramètres puis **SSH KEYS** dans l’espace personnel Gitlab. 


- Désormais, grâce à la liaison d’un dossier de mon ordinateur avec Gitlab directement, je peux ajouter du texte et de photos, il se synchronisent directement en encodant dans le terminal: 


```git pull``` puis ```git add -A ```et ```git commit -m ```et enfin ```git push```

Le terminal affiche alors: **MacBook-Pro-de-Valere:valere.santarelli valeresantarelli$ git push
Enter passphrase for key '/Users/valeresantarelli/.ssh/id_ed25519':**
 

Les problèmes commencent...

```
To gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/valere.santarelli.git
! [rejected]        main -> main (non-fast-forward)
error: impossible de pousser des références vers 'gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/valere.santarelli.git'
astuce: Les mises à jour ont été rejetées car la pointe de la branche courante est derrière
astuce: son homologue distant. Intégrez les changements distants (par exemple 'git pull ...')
astuce: avant de pousser à nouveau.
astuce: Voir la 'Note à propos des avances rapides' dans 'git push --help' pour plus d'information.
MacBook-Pro-de-Valere:valere.santarelli valeresantarelli$

```

A ce stade je n'arrive plus a enregistrer mes modifications a partir du terminal, mais seulement directement dans Git.

Je commence a comprendre:

![](../images/docuu2.png)

## 2.DOCUMENTATION DE MON PROFIL

J’ai ensuite complété mon profil personnel et intégré des photos grace aux outils **repository et edition** directement dans Gitlab.
**valere.santarelli / docs / Edit**


![](../images/moi10.png)



## Liens utiles



- [Markdown](https://en.wikipedia.org/wiki/Markdown)
- [GIT LAB](https://about.gitlab.com/fr-fr/)
- [DOCUMENTING YOUR WORK](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/basics/-/blob/master/documentation.md#why-is-documentation-important-)



