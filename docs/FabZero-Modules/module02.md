# Module 2. Conception Assistée par Ordinateur

Cette semaine nous avons appris a utiliser le **logiciel fusion 360**, c'est un programme de **conception 3D**. Il est téléchargeable en version étudiant directement sur la plateforme Autodesk et fonctionne sous Mac et Windows.
Pour rappel, j'ai choisi comme objet un vide poche en bois. 

Dans un premier temps nous avons dessiné un objet commun en direct avec l'enseignant, un cube extrudé.
Puis, nous sommes passés à la conception de notre objet personnel.

## Recherches

J'ai tout d'abord **analysé mon objet** pour comprendre sa construction puis je l'ai **décomposé en plusieurs parties**.
Sachant que mon objet est **réctiligne**, j'ai **créer une trame** qui suivait le **calpinage** de celui ci, pour m'aider lord du dessin 3D et travailler plus méthodiquement. 
J'ai aussi regardé des videos sur Youtube expliqueant les premiers pas sur le programme. 



## PARTIE 01, Le cube

- Pour apprendre les bases de **Fusion 360**, on commence a faire un cube. Voici les étapes.

-Creer un carré

-Extruder le carré pour creer un cube

-Arrondir les bords

-Creer des percements

-Dupliquer les percements

-Extraire de la matière

![](../images/cube10.png)


- Voici mes premiers essais de rendus de mon cube, en testant divers types matériaux. Cela se fait dans l'onglet RENDER du logiciel.

![](../images/B.png)
![](../images/A.png)



## PARTIE 02, Le vide poche

Ici je m'attaque à la conception de mon objet perso.

Je me place donc sur l’origine pour commencer mon esquisse.
je dessine mon objet en plan, je m'aide de la trame et du calpinage de mon objet pour me repérer. 
Je rentre les cotes exactes.
Cela fait, j’appui sur "terminer l’esquisse" pour passer à l'étape en 3D. 

Je prends l'outil ''extruder'', je remonte mes faces à leur bonne hauteurs.



![](../images/listevidepoche.png)



## Model 3D

Voila mon modèle 3D avec un Render Fusion, objectif de la journée atteint, j'ai pu prendre en mains le logiciel et échanger avec d'autres étudiants.

![](../images/c.png)


## Liens utiles

- _VIDE POCHE DXF_
![vide poche dxf](../images/videpochefusion.dxf)

- _VIDE POCHE STL_
![VIDE POCHE STL](../images/videpochestl.stl)

- [YOUTUBE TUTO](http://youtube.com/watch?v=bh13sZN4TPc)

- [FUSION 360](https://www.autodesk.com/products/fusion-360/free-trial)



